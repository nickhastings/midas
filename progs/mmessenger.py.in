#!@Python3_EXECUTABLE@

#CMake will set the path to python3 above

import midas.client
import requests

"""
JTK McKenna

A simple program that connects to a midas experiment, and sends HTTP 
requests messages (to discord, mattermost and or slack) via webhook

"""

# Create a dictionary of all the MIDAS Message Types
MT_DICT = {}
MT_DICT[midas.MT_ERROR] = "MT_ERROR"
MT_DICT[midas.MT_INFO] = "MT_INFO"
MT_DICT[midas.MT_DEBUG] = "MT_DEBUG"
MT_DICT[midas.MT_USER] = "MT_USER"
MT_DICT[midas.MT_LOG] = "MT_LOG"
MT_DICT[midas.MT_TALK] = "MT_TALK"
MT_DICT[midas.MT_CALL] = "MT_CALL"


# Create a list of the MT_DICT strings
MT_MessageTypeList = []
for key in MT_DICT.keys():
    MT_MessageTypeList.append(MT_DICT[key])


def PostToDiscord(MsgType, message, author, URL_DICT):
    # Loop up MsgType (int) in dictionary for string... 
    key = MT_DICT[MsgType]
    # Get URL for this message type
    webhook_url = URL_DICT[key]
    # If key returns a webhook URL
    if len(webhook_url):
        # Format message
        discord_message = {
            'content': message.decode('UTF-8'),
            'username': author
        }
        result = requests.post(
            webhook_url, json=discord_message)
        try:
            result.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print('Discord webhook error: %s' % err)
        else:
            print('Discord webhook response: %s' % result.status_code)


def PostToMattermost(MsgType, message,author,URL_DICT):
    # Loop up MsgType (int) in dictionary for string... 
    key = MT_DICT[MsgType]
    # Get URL for this message type
    webhook_url = URL_DICT[key]
    # If key returns a webhook URL
    if len(webhook_url):
        # Format message
        mattermost_message = {
            "text": message.decode('UTF-8'),
            'username': author
        }
        result = requests.post(
            webhook_url, json=mattermost_message)
        try:
            result.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print('mattermost webhook error: %s' % err)
        else:
            print('mattermost webhook response: %s' % result.status_code)


def PostToSlack(MsgType, message,URL_DICT):
    # Loop up MsgType (int) in dictionary for string... 
    key = MT_DICT[MsgType]
    # Get URL for this message type
    webhook_url = URL_DICT[key]
    # If key returns a webhook URL
    if len(webhook_url):
        #Username is imbedded into the slack app, so can't be given here
        slack_message = {
            "text": message.decode('UTF-8')
        }
        result = requests.post(
            webhook_url, json=slack_message)
        try:
            result.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print('slack webhook error: %s' % err)
        else:
            print('slack webhook response: %s' % result.status_code)


if __name__ == "__main__":
    client = midas.client.MidasClient("mmessenger")
    
    # Read the name of the experiment, use it as 'Author' in posts
    ExperimentName = client.odb_get("/Experiment/Name")

    # Read all webhook urls (and make entries if they dont exist)
    for service in [ "Discord", "Mattermost", "Slack" ]:
        for key in MT_MessageTypeList:
            odb_path = "/Messenger/" + service + "/WebHooks/" + key
            if not client.odb_exists(odb_path):
                # default entry for webhook URL is no text (len = 0)
                client.odb_set(odb_path,"")
    
    discord = {}
    mattermost = {}
    slack = {}
    # Populate dictionary of webhook URLs
    for key in MT_MessageTypeList:
        discord[key]    = client.odb_get("/Messenger/" + "Discord" + "/WebHooks/" + key)
        mattermost[key] = client.odb_get("/Messenger/" + "Mattermost" + "/WebHooks/" + key)
        slack[key]      = client.odb_get("/Messenger/" + "Slack" + "/WebHooks/" + key)

    # Define which buffer we want to listen for events on (SYSTEM is the 
    # main midas buffer).
    buffer_handle = client.open_event_buffer("SYSMSG")

    # Request events from this buffer that match certain criteria. In this
    # case we will only be told about events with an "event ID" of 14.
    request_id = client.register_event_request(buffer_handle)

    while True:
        # If there's an event ready, `event` will contain a `midas.event.Event`
        # object. If not, it will be None. If you want to block waiting for an
        # event to arrive, you could set async_flag to False.
        event = client.receive_event(buffer_handle, async_flag=True)
        
        if event is not None:
            # Print some information to screen about this event.
            bank_names = ", ".join(b.name for b in event.banks)
            print("Received event with timestamp %s containing banks %s" % (event.header.timestamp, bank_names))
            if event.header.is_msg_event():
                # Strip off junk data after string
                message = event.non_bank_data[0:event.header.event_data_size_bytes - midas.event.event_header_size - 1]
                print(str(message))

                for MsgType in [midas.MT_ERROR, midas.MT_INFO, midas.MT_DEBUG, midas.MT_USER, midas.MT_LOG, midas.MT_TALK, midas.MT_CALL]:
                    if event.header.trigger_mask & MsgType:
                        PostToDiscord(MsgType, message, ExperimentName, discord)
                        PostToMattermost(MsgType, message, ExperimentName, mattermost)
                        # Author is not part of the slack message
                        PostToSlack(MsgType, message, slack)

        # Talk to midas so it knows we're alive, or can kill us if the user
        # pressed the "stop program" button.
        client.communicate(10)

    # You don't have to cancel the event request manually (it will be done
    # automatically when the program exits), but for completeness we're just
    # showing that such a function exists.
    client.deregister_event_request(buffer_handle, request_id)
    
    ##################################################################
    
    # Disconnect from midas before we exit
    client.disconnect()
