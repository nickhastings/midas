cmake_minimum_required(VERSION 3.0)
project(epics_fe)

# Check for MIDASSYS environment variable
if (NOT DEFINED ENV{MIDASSYS})
   message(SEND_ERROR "MIDASSYS environment variable not defined.")
endif()
if (NOT DEFINED ENV{EPICSSYS})
   message(SEND_ERROR "EPICSSYS environment variable not defined.")
endif()

set(CMAKE_CXX_STANDARD 11)
set(MIDASSYS $ENV{MIDASSYS})
set(EPICSSYS $ENV{EPICSSYS})

if (${CMAKE_SYSTEM_NAME} MATCHES Linux)
   link_directories(${EPICSSYS}/lib/linux-x86_64)
   set(LIBS ${LIBS} -lpthread -lutil -lrt -lbsd -lca -ldl)
endif()
if (${CMAKE_SYSTEM_NAME} MATCHES Darwin)
   link_directories(${EPICSSYS}/lib/darwin-aarch64)
   set(LIBS ${LIBS} -lutil -lca)
endif()

add_executable(epics_fe epics_fe.cxx
   ${MIDASSYS}/src/mfe.cxx
   ${MIDASSYS}/src/mfed.cxx
   ${MIDASSYS}/src/odbxx.cxx
   ${MIDASSYS}/drivers/class/generic.cxx
   ${MIDASSYS}/drivers/device/psi_epics.cxx
   )

target_include_directories(epics_fe PRIVATE
   ${MIDASSYS}/include
   ${MIDASSYS}/mscb/include
   ${MIDASSYS}/drivers
   ${MIDASSYS}/mxml
   ${EPICSSYS}/include
   ${EPICSSYS}/include/os/Linux
   ${EPICSSYS}/include/os/Darwin
   ${EPICSSYS}/include/compiler/gcc
   ${EPICSSYS}/include/compiler/clang
   )

target_link_libraries(epics_fe
   ${MIDASSYS}/lib/libmfe.a
   ${MIDASSYS}/lib/libmidas.a
   ${LIBS}
   )
